
function sample_refractive(config::Config,  normal::Vec4d, wo::Vec4d)
    # Ideal dielectry REFRACTION
    reflDir = wo - normal * 2.0 * dotproduct(normal, wo)
    #nl = normal if normal.dot(wo) < 0 else normal * -1
    if dotproduct(normal, wo) < 0.0
        nl = normal
    else
        nl = normal * -1.0
    end
    #into = normal.dot(nl) > 0 # Ray from outside going in?
    into = (dotproduct(normal, nl) > 0.0)
    nc = 1.0
    nt = 1.5
    #nnt = nc/nt if into else nt/nc
    if into
        nnt = nc/nt
    else
        nnt = nt/nc
    end
    ddn = dotproduct(wo, nl) 
    cos2t = 1.0 - nnt ^ 2 * (1.0 - ddn ^ 2)

    if cos2t < 0.0
        # total internal reflection
        return reflDir, 1.0
    else
        #tdir = (wo*nnt - normal*((1 if into else -1)*(ddn*nnt + math.sqrt(cos2t)))).norm()
        if into
            tdir = (wo*nnt - normal*( 1.0*(ddn*nnt + sqrt(cos2t))))
        else
            tdir = (wo*nnt - normal*(-1.0*(ddn*nnt + sqrt(cos2t))))
        end
        normalise!(tdir)

        a = nt - nc
        b = nt + nc
        R0 = a*a / (b*b)
        #c = 1.0-(-ddn if into else tdir.dot(normal))
        if into
            c = 1.0-(-ddn)
        else
            c = 1.0-(dotproduct(tdir, normal))
        end

        Re = R0 + (1.0-R0)* (c ^ 5)
        Tr = 1.0-Re
        P  = 0.25 + 0.5 * Re
        RP = Re/P
        TP = Tr/(1.0 - P)

        if rand() < P
            return reflDir, RP
        else
            return tdir, TP
        end
    end
end
