require("Object.jl")
require("Config.jl")
require("Ray.jl")

type Intersection
    object::Union(Object, Nothing)
    ray::Ray
    distance::Float64
    entering::Bool
end


function contact_point_and_normal(config::Config, intersect::Intersection)
    contact_point = config.contact_point

    object   = intersect.object
    ray      = intersect.ray
    distance = intersect.distance

    # contact_point = ray.origin + 0.999999999 * distance * ray.direction
    tmp = 0.999999999 * distance
    for i in 1:4
        contact_point[i] = ray.origin[i] + tmp * ray.direction[i]
    end
    normal = contact_normal(config, object, contact_point, intersect)

    return contact_point, normal
end

