#!/usr/bin/env julia

typealias Vec4d Array{Float64,1}
typealias Mat4d Array{Float64,2}

function neg!(v::Vec4d)
    # Inline negation of a vector
    @assert length(v) == 4 # Length 4 vectors only
    @assert v[4] == 0.0    # Must be a vector, not a point
    for i in 1:3
        @inbounds v[i] = -v[i]
    end
    v
end

function matrix_vector_mul!(M::Mat4d, v::Vec4d, out::Vec4d)
    # Custom matrix vector multiply used to apply transform matrices
    # out - pre-allocated output vector
    #
    # I've tried sprinking @inbounds through here but didn't see any speed up
    # also tried manually unrolling and accessing M[] in memory order
    # also tried two nested loops (over rows, and cols)

    @inbounds out[1] = M[1] * v[1] + M[5] * v[2] + M[ 9] * v[3] + M[13] * v[4];
    @inbounds out[2] = M[2] * v[1] + M[6] * v[2] + M[10] * v[3] + M[14] * v[4];
    @inbounds out[3] = M[3] * v[1] + M[7] * v[2] + M[11] * v[3] + M[15] * v[4];
    @inbounds out[4] = M[4] * v[1] + M[8] * v[2] + M[12] * v[3] + M[16] * v[4];
    return out
end

function len(v::Vec4d)
    @assert length(v) == 4
    @assert v[4] == 0.0
    s = 0.0
    for i in 1:3
        @inbounds s += v[i] * v[i]
    end
    s = sqrt(s)
    s
end

function normalise!(v::Vec4d)
    # in-place normalise
    @assert length(v) == 4
    @assert v[4] == 0.0    # Must be a vector, not a point
    n = len(v)
    for i in 1:3
        @inbounds v[i] /= n
    end
    v
end

function normalise_and_get_length!(v::Vec4d)
    # in-place normalise which returns the length
    @assert length(v) == 4
    @assert v[4] == 0.0
    n = len(v)
    for i in 1:3
        @inbounds v[i] /= n
    end
    n
end

function crossproduct(a::Vec4d, b::Vec4d)
    @assert length(a) == 4
    @assert a[4] == 0.0
    @assert length(b) == 4
    @assert b[4] == 0.0
    [a[2]*b[3] - a[3]*b[2], a[3] * b[1] - a[1] * b[3], a[1] * b[2] - a[2] * b[1], 0]
end

function crossproduct!(a::Vec4d, b::Vec4d, out::Vec4d)
    # Cross product with pre-allocated output
    @assert length(a) == 4
    @assert a[4] == 0.0
    @assert length(b) == 4
    @assert b[4] == 0.0
    @inbounds out[1] = a[2] * b[3] - a[3] * b[2]
    @inbounds out[2] = a[3] * b[1] - a[1] * b[3]
    @inbounds out[3] = a[1] * b[2] - a[2] * b[1]
    @inbounds out[4] = 0.0
    out
end

function dotproduct(a::Vec4d)
    @assert length(a) == 4
    @assert a[4] == 0.0
    s = 0.0
    for i in 1:3
        @inbounds s += a[i] * a[i]
    end
    s
end

function dotproduct(a::Vec4d, b::Vec4d)
    @assert length(a) == 4
    @assert a[4] == 0.0
    @assert length(b) == 4
    @assert b[4] == 0.0
    s = 0.0
    for i in 1:3
        @inbounds s += a[i] * b[i]
    end
    s
end

