#!/usr/bin/env julia 
require("Vector.jl")
require("Transform.jl")
require("Intersection.jl")

require("Object.jl")
require("Sphere.jl")
require("Cube.jl")
require("HalfSpace.jl")
require("CSG.jl")
require("AABB.jl")

require("Config.jl")

require("render.jl")

# Python runtime for 400,3,2 = 11 seconds
# Julia runtime as I've optimised
# 52, 49, 40, 33, 11 - I win!

# run with "julia -p 8 main.jl" if you want parallel rendering
if true
    @everywhere start_config = setup(400, 3, 2)
    println("Starting...")
    tic()
    image = render(start_config.rows, start_config.cols)
    toc()
    write_png(image, start_config.rows, start_config.cols)
else
    start_config = setup(400, 3, 2)
    @profile image = render(start_config.rows, start_config.cols)
    write_png(image, start_config.rows, start_config.cols)
    Profile.print()
    println("================================================================")
    println("================================================================")
    Profile.print(format=:flat)
end
