#!/usr/bin/env julia

immutable Transform
    mat::Mat4d
    inv::Mat4d
end

function *(t1::Transform, t2::Transform)
    # Combine two transforms. Both forwards and inverse
    Transform(t1.mat * t2.mat, t1.inv * t2.inv)
end

function -(t1::Transform)
    # The unary negative of a Transform is the inverse transform
    Transform(t1.inv, t1.mat)
end


function create_transform(ttype::ASCIIString="identity", value::Any=None)
    # By default, matrix and its inverse are the identity matrix. i.e. do nothing
    mat = eye(4)
    inv = eye(4)
    if false
        println("=====================")
        println(ttype)
        println(value)
        println("=====================")
    end
    if ttype == "identity"
        # do nothing
    elseif ttype == "invert"
        assert(typeof(value) == Transform)
        mat = value.inv
        inv = value.mat
    elseif ttype == "translate"
        assert(typeof(value) == Vec4d)
        for i in 1:3
            mat[i, 4] =  value[i]
            inv[i, 4] = -value[i]
        end
    elseif ttype == "scale"
        for i in 1:3
            mat[i, i] =     value
            inv[i, i] = 1.0/value
        end
    elseif ttype == "xangle"
        sin_t = sin(deg2rad(value))
        cos_t = cos(deg2rad(value))
        mat[2,2] =  cos_t
        mat[2,3] = -sin_t
        mat[3,2] =  sin_t
        mat[3,3] =  cos_t
        inv = transpose(mat) 
    elseif ttype == "yangle"
        sin_t = sin(deg2rad(value))
        cos_t = cos(deg2rad(value))
        mat[1,1] =  cos_t
        mat[1,3] =  sin_t
        mat[3,1] = -sin_t
        mat[3,3] =  cos_t
        inv = transpose(mat) 
    elseif ttype == "zangle"
        sin_t = sin(deg2rad(value))
        cos_t = cos(deg2rad(value))
        mat[1,1] =  cos_t
        mat[1,2] = -sin_t
        mat[2,1] =  sin_t
        mat[2,2] =  cos_t
        inv = transpose(mat) 
    else
        throw(Exception())
    end
    return Transform(mat, inv) 
end

if false
    m = create_transform()
    println(m)
    println(typeof(m))
    n = create_transform("invert", m)
    println(m)

    m = create_transform("translate", [0.0, 1.0, 2.0])
    println(m)
    println(typeof(m))

    m = create_transform("scale", 2)
    println(m)
    println(typeof(m))

    for ttype in ["xangle", "yangle", "zangle"]
        m = create_transform(ttype, 30)
        println(m)
        println(typeof(m))
    end
end









