#!/usr/bin/env julia

require("Transform.jl")
require("Ray.jl")
require("Object.jl")
require("Config.jl")
require("Intersection.jl")

type Sphere <: Object
    colour::Vec4d
    specularity::Float64
    refractive::Bool
    object2world::Transform
    world2object::Transform
    is_light::Bool
    Sphere(colour::Vec4d, object2world::Transform                ) = new(colour, 0.0, false, object2world, -object2world, false   )
    Sphere(colour::Vec4d, object2world::Transform, is_light::Bool) = new(colour, 0.0, false, object2world, -object2world, is_light)
    Sphere(colour::Vec4d, refractive::Bool, object2world::Transform) = new(colour, 0.0, refractive, object2world, -object2world, false)
end

# CSG ready intersect function. return near/far intersect distance of (-inf, -inf)
function intersect(config::Config, self::Sphere, ray::Ray)
    # Use pre-allocated vectors, and do calculations inline
    ray_origin    = config.ray_origin
    ray_direction = config.ray_direction
    matrix_vector_mul!(self.world2object.mat, ray.origin   , ray_origin)
    matrix_vector_mul!(self.world2object.mat, ray.direction, ray_direction)
    ray_length = normalise_and_get_length!(ray_direction)

    ray_origin[4] = 0.0 # treat as vector

    A = dotproduct(ray_direction)
    B = 2.0 * dotproduct(ray_origin, ray_direction)
    C = dotproduct(ray_origin) - 0.25
    det = B*B - 4.0 * A * C
    if det < 0.0
        return nothing, nothing
    elseif det == 0.0
        # tangentail intersection
        # ignored for now
        return nothing, nothing
    else
        # 2 intersections possible
        t0 = (-B - sqrt(det)) / (2.0 * A)
        t1 = (-B + sqrt(det)) / (2.0 * A)
        return Intersection(self, ray, t0 / ray_length, true), Intersection(self, ray, t1 / ray_length, false)
    end
end

function contact_normal(config::Config, self::Sphere, contact_point::Vec4d, this_intersect::Intersection)
    object_normal = config.object_normal
    world_normal  = config.world_normal
    matrix_vector_mul!(self.world2object.mat, contact_point, object_normal)
    object_normal[4] = 0.0
    matrix_vector_mul!(self.object2world.mat, object_normal  , world_normal)

    if this_intersect.entering
        return normalise!(world_normal)
    else
        return normalise!(neg!(world_normal))
    end
end

function random_point_on_xx(config::Config, self::Sphere)
    # simulate point source
    return self.object2world.mat * [0.0, 0.0, 0.0, 1.0]
end

function random_point_and_direction_on_light(config::Config, self::Sphere)
    """ point in spherical coordinates p(q,f)
    point in cartesian coords p(x,y,z)
    x = r cos(q) sin(f)
    y = r sin(q) sin(f)
    z = r cos(f)
    Random sampling of q, f coords then convert to x,y,z
    """
    u = rand()
    v = rand()
    q = 2.0 * pi * u
    f = acos(2*v - 1)
    r = 0.5

    # tried shifting to static allocation here. slows down code! 
    objectspace_point = [0.0, 0.0, 0.0, 0.0]
    objectspace_point[1] = r * cos(q) * sin(f)
    objectspace_point[2] = r * sin(q) * sin(f)
    objectspace_point[3] = r * cos(f)
    objectspace_point[4] = 1.0

    random_point = self.object2world.mat * objectspace_point
    objectspace_point[4] = 0.0 # convert to a vector
    random_direction = self.object2world.mat * objectspace_point
    
    return random_point, random_direction
end

