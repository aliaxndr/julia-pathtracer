#!/usr/bin/env julia

function write_png(image, rows::Int64, cols::Int64)
    # World's dodgiest PNG output
    # write out as netpbm PGM format
    # then use imagemagick to convert to PNG

    # get the maximum value, and scale the output by this
    maxval = maximum([maximum(image[row][col]) for row in 1:rows, col=1:cols])
    minval = minimum([minimum(image[row][col]) for row in 1:rows, col=1:cols])
    println("MAXVAL: ", maxval, "  MINVAL: ", minval)

    outfile = open("images/out.pgm", "w")
    println(outfile, "P3") # plain PPM
    print(outfile, cols)
    print(outfile, " ")
    println(outfile, rows)
    println(outfile, 255)
    for row in 1:rows
        for col in 1:cols
            #println(round(255.0 * image[row][col]))
            for idx in 1:3
                println(outfile, convert(Uint8, min(255.0, round(255.0 * (image[row][col][idx]-minval) / (maxval-minval)))))
            end
        end
    end
    close(outfile)

    # And now use imagemagick to convert pgm to png
    run(`convert images/out.pgm images/out.png`)
    run(`rm images/out.pgm`)
end
