julia-pathtracer
================
A pathtracer in julialang.

Run:  julia ./main.jl

![Depth of field](https://bitbucket.org/mikefc/julia-pathtracer/raw/master/hiqual-dof.jpg)
![Example](https://bitbucket.org/mikefc/julia-pathtracer/raw/master/hiqual.jpg)

python vs julia
===============
* python and julia renderers use identical algorithms/techniques.
* To get julia code running fast, all implicit vector ops needed to be unrolled and turned into explicit loops.
* Julia code is now faster than the python + C code (before addition of CSG to julia code)
* Julia code will actually run easily in parallel using pmap. Running on a 4 Core machine, get about 3.5x speedup over single core.
* Python's muliprocessing/pmap won't work because CData (where all the matrix and vector data is stored) cannot be pickled. So python code remains single core.

ToDo
====
* add a materials type
* refraction
* bidirectional path tracing

Features
========
* Constructive solid geom
* Depth-of-field
* Diffuse and Specular reflections

CSG
===
* In the process of adding constructive solid geometry (CSG).
* Code will get slower - merging of intersections for a CSG object result in a big performance hit.
* e.g. intersect functions have to calculate and return 2 values (near and far intersect distance) when they hit a CSG object
* Simple CSGs with just 2 objects INTERSECT/DIFFERENCE/UNION work 

optimising
==========
* Went through [Performance Tips Page](http://julia.readthedocs.org/en/latest/manual/performance-tips/)
* wrote everything with types from the start. i.e. all function calls have types, and nearly all composite types have fully typed fields.
* config variables moved from global to a Config type. Nice speedup achieved.
* made types immutable where possible. no speedup.
* experimented with @inbounds in core places (e.g. matrix_vector_mul). no speedup
* experimented with operation order in matrix_vector_mul. no speedup
* functions are mostly "type stable"
* i think i've totally avoided changing the type of a variable
* pre-allocating outputs
    * I've preallocated memory for intersect() and contact_normal() methods. These are the functions that get called the most.  => some speedup.
* use Devectorize in a few places. 2 seconds speedup (33s => 31s)
* remove the Intersection type and using a tuple instead. No speedup
* pre-allocated memeory for sample_diffuse. (31s => 27s)
* avoid intermediate memory during calculations in sample_diffuse. Maybe 0.5s speedup. (27s => 26.5s).  Makes the code heaps uglier though
* manually devectorize all the render functions => WIN!  27s => 11s

default scene
==============
![Default scene](https://bitbucket.org/mikefc/julia-pathtracer/raw/master/default.jpg)
