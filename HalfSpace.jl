#!/usr/bin/env julia

require("Vector.jl")
require("Ray.jl")
require("Object.jl")
require("Config.jl")
require("Intersection.jl")

type HalfSpace <: Object
    colour::Vec4d
    specularity::Float64
    refractive::Bool
    object2world::Transform
    world2object::Transform
    is_light::Bool
    HalfSpace(colour::Vec4d, object2world::Transform) = new(colour, 0.0, false, object2world, -object2world, false)
end

function intersect(config::Config, self::HalfSpace, ray::Ray)
    # Use pre-allocated vectors, and do calculations inline
    ray_origin    = config.ray_origin
    ray_direction = config.ray_direction
    matrix_vector_mul!(self.world2object.mat, ray.origin   , ray_origin)
    matrix_vector_mul!(self.world2object.mat, ray.direction, ray_direction)
    ray_length = normalise_and_get_length!(ray_direction)

    v0 = -ray_origin[2]

    if v0 < 0.0
        l = ray_direction[2]
        if l == 0.0
            return nothing, nothing
        end
        t = v0 / l
        if l < 0
            # ray is moving into halfplane
            return Intersection(self, ray, t/ray_length, true), Intersection(self, ray, inf(Float64), false)
        end
        if l > 0
            # ray is moving out from halfplane
            return Intersection(self, ray, -inf(Float64), true), Intersection(self, ray, t/ray_length, false)
        end
    end
    return nothing, nothing
end

function contact_normal(config::Config, self::HalfSpace, contact_point::Vec4d, this_intersect::Intersection)
    # plane normal in object space is the y unit vector
    world_normal = config.world_normal
    matrix_vector_mul!(self.object2world.mat, config.yvec, world_normal)
    if this_intersect.entering
        return normalise!(world_normal)
    else
        return normalise!(neg!(world_normal))
    end
end
