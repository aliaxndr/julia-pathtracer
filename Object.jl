#!/usr/bin/env julia
require("Ray.jl")
require("Vector.jl")
abstract Object

# Moving the generic object
# transform code into it's own function gave about a 7.5s INCREASE in RUNTIME. Booooo!
function transform_ray_into_object_space(config, self::Object, ray::Ray)
    ray_origin    = config.ray_origin
    ray_direction = config.ray_direction
    matrix_vector_mul!(self.world2object.mat, ray.origin   , ray_origin)
    matrix_vector_mul!(self.world2object.mat, ray.direction, ray_direction)
    ray_length = normalise_and_get_length!(ray_direction)
    ray_origin, ray_direction, ray_length
end
