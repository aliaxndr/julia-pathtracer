#!/usr/bin/env julia

require("Ray.jl")

const MERGE_INTERSECT  = 0::Int64
const MERGE_UNION      = 1::Int64
const MERGE_DIFFERENCE = 2::Int64

const OBJECT_L = true
const OBJECT_R = false

# keep it simple at the start. 2 objects only
immutable CSG <: Object
    colour::Vec4d
    object2world::Transform # CSG can have its own tranform
    world2object::Transform
    is_light::Bool
    objects::(Object, Int64, Object)

    CSG(colour::Vec4d, object2world::Transform, objects::(Object, Int64, Object)) = new(colour, object2world, -object2world, false, objects)
end

function prepare_CSG_objects(self::CSG)
    # pre-multiply transform matrices of all objects which make up this CSG object
    object1, merge_type, object2 = self.objects
    object1.object2world = object1.object2world * self.object2world
    object1.world2object = object1.world2object * self.world2object
    object2.object2world = object2.object2world * self.object2world
    object2.world2object = object2.world2object * self.world2object
end


function intersect(config::Config, self::CSG, ray::Ray)
    # The ray leaving or entering an object is an event
    events = (Float64, Intersection, Bool)[]

    # for more than 2 objects, self.objects is actually going to be a tree
    object1, merge_type, object2 = self.objects

    intersectA1, intersectA2 = intersect(config, object1, ray)
    # Short circuit operations
    if merge_type == MERGE_INTERSECT || merge_type == MERGE_DIFFERENCE
        if intersectA1 == nothing
            # Can't have an intersect if it misses one of the objects
            # can't have a difference if it misses the A object
            return nothing, nothing
        end
    end

    intersectB1, intersectB2 = intersect(config, object2, ray)
    # Short circuit operations
    if merge_type == MERGE_INTERSECT
        if intersectB1 == nothing
            # Can't have an intersect if it misses one of the objects
            return nothing, nothing
        end
    elseif merge_type == MERGE_UNION
        if intersectA1 == nothing
            return intersectB1, intersectB2
        end
        if intersectB1 == nothing
            return intersectA1, intersectA2
        end
    elseif merge_type == MERGE_DIFFERENCE
        if intersectB1 == nothing
            return intersectA1, intersectA2
        end
    end

    @assert intersectA1 != nothing
    @assert intersectB1 != nothing

    push!(events, (intersectA1.distance, intersectA1, OBJECT_L))
    push!(events, (intersectA2.distance, intersectA2, OBJECT_L))
    push!(events, (intersectB1.distance, intersectB1, OBJECT_R))
    push!(events, (intersectB2.distance, intersectB2, OBJECT_R))

    sort!(events) # by distance

    inA  = false
    inB  = false
    last_output = false
    final_intersects = Intersection[]

    for (distance, this_intersect, obj) in events
        if obj == OBJECT_L
            inA = !inA
        else
            inB = !inB
        end
        if merge_type == MERGE_UNION
            output = inA || inB
        elseif merge_type == MERGE_INTERSECT
            output = inA && inB
        elseif merge_type == MERGE_DIFFERENCE
            output = inA && !inB
        end

        if length(final_intersects) == 0
            if output
                push!(final_intersects, this_intersect)
            end
        elseif output != last_output
            push!(final_intersects, this_intersect)
        end
        last_output = output
    end

    # Search nearest and return. Do this if this is the root tree node
    for (idx, this_intersect) in enumerate(final_intersects)
        # need to consider the intersects in pairs (in, out) 
        if idx%2 == 1 && this_intersect.distance > 0.0
            return this_intersect, nothing
        end
    end

    return nothing, nothing
end



