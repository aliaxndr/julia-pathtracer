#!/usr/bin/env julia

immutable Ray
    origin   ::Vec4d
    direction::Vec4d
end
