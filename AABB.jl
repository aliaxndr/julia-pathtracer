#!/usr/bin/env julia

require("Transform.jl")
require("Ray.jl")
require("Object.jl")
require("Config.jl")
require("Intersection.jl")

type AABB <: Object  
    # Axis aligned unit cube in world space.
    # eventually: defined upper/lower extents (again, in world space)
    colour::Vec4d
    object2world::Transform
    world2object::Transform
    is_light::Bool
    AABB() = new([1.0, 0.8, 0.6, 1], create_transform(), create_transform(), false)
end

function intersect(config::Config, self::AABB, ray::Ray)
    # Use pre-allocated vectors, and do calculations inline
    ray_origin    = ray.origin
    ray_direction = ray.direction
    ray_length = 1.0

    tnear = -inf(Float64)
    tfar  =  inf(Float64)
    for i in 1:3
        swap = false
        if ray_direction[i] == 0.0
            # ray is parallel to this axis
            if abs(ray_origin[i]) > 0.5
                # and misses the cube entirely
                return nothing, nothing
            end
        else
            # Ray is not parallel to this axis
            t1 = (-0.5 - ray_origin[i]) / ray_direction[i]
            t2 = ( 0.5 - ray_origin[i]) / ray_direction[i]
            if t1 > t2
                t1, t2 = t2, t1
                swap = true
            end
            if t1 > tnear
                tnear = t1
                hit_swap  = swap
                hit_plane = i
            end
            if t2 < tfar
                tfar  = t2
            end
            if tnear > tfar
                # Box is missed
                return nothing, nothing
            end
            if tfar < 0
                # Box is behind ray_
                return nothing, nothing
            end
        end
    end
    return Intersection(self, tnear/ray_length, true), Intersection(self, tfar/ray_length, false)
end

function contact_normal(config::Config, self::AABB, contact_point::Vec4d, this_intersect::Intersection)
    # Convert the contact point into a normal in one of the 6 possible directions
    # find the abs(maximum) value. 
    #    - Its index tells you the axis.
    #    - Its sign indicates the direction

    # !!!!!!!!!!!!!!!!!!!
    # this is only if AABB is unit cube at (0, 0, 0)
    # !!!!!!!!!!!!!!!!!!!!

    maxval = -inf(Float64)
    maxi    = 1
    maxsign = 1
    for i in 1:3
        if abs(contact_point[i]) > maxval
            maxval = abs(contact_point[i])
            maxi    = i
            maxsign = sign(contact_point[i])
        end
    end
    world_normal = config.world_normal
    for i in 1:4
        world_normal[i] = 0
    end
    world_normal[maxi] = maxsign

    if this_intersect.entering
        return world_normal
    else
        return neg!(world_normal)
    end
end

